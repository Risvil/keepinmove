Welcome to Keep-In Move!
=====================

### Disclaimer: This is a work in progress. ###

**Keep-In Move (KIM)** provides a multi-touch gesture library and interface to client applications. Compatible with any TUIO generating aplications, such as Keep-In Touch (https://bitbucket.org/Risvil/keepintouch). 

There are a set of sample applications to help you get started using KIM:


*MTImageViewer* - Simulates a table containing stacked images. Users can move, rotate and scale each image simultaneously and independently. 
https://bitbucket.org/Risvil/mtimageviewer

*MTPainter* - Simple finger painting application. 
https://bitbucket.org/Risvil/mtpainter

*MTWoodenPuzzle* - Wooden piece puzzle like the ones little childs plays with. Each piece can be manipulated (moved, rotated, scaled) simultaneously and independently.
https://bitbucket.org/Risvil/mtwoodenpuzzle
----------