#include <memory>
#include <iostream>
#include <forward_list>

#include "kim.h"
#include "TUIOClient/TUIOClient.h"

namespace kim
{

class Engine : public IEngine, public ITUIOEventListener
{
public:
	Engine();

	// IEngine interface
	virtual bool Init() override;
	virtual void Run() override;
	virtual void Shutdown() override;

	virtual void RegisterRegion(Region* region, Priority priority) override;
	virtual void UnregisterRegion(Region* region) override;

	// ITUIOEventListener interface
	virtual void OnCursorBirth(const TUIOCursor& cursor) override;
	virtual void OnCursorUpdate(const TUIOCursor& cursor) override;
	virtual void OnCursorDeath(const TUIOCursor& cursor) override;
	virtual void OnRefresh() override;

private:
	std::shared_ptr<ATUIOClient> m_pTUIO;

	std::forward_list<Region*> m_frontRegions;
	std::forward_list<Region*> m_backgroundRegions;
};

std::shared_ptr<IEngine> IEngine::CreateEngine()
{
	return std::make_shared<Engine>();
}

Engine::Engine()
: m_pTUIO(ATUIOClient::CreateTUIOClient())
{
}

bool Engine::Init()
{
	return m_pTUIO->Init();
}

void Engine::Run()
{
	m_pTUIO->RegisterListener(this);
	m_pTUIO->Run();
}

void Engine::Shutdown()
{
	m_pTUIO->Shutdown();
}

void Engine::OnCursorBirth(const TUIOCursor& cursor)
{
	bool bProcessed = false;

	TouchPoint point = cursor;
	for (auto& region : m_frontRegions)
	{
		if (region != nullptr && region->OnPointBirth(point))
		{
			// Put this region at the top of the list
			m_frontRegions.emplace_front(region);
			m_frontRegions.unique();
			bProcessed = true;
			break;
		}
	}

	if (!bProcessed)
	{
		for (auto& region : m_backgroundRegions)
		{
			if (region != nullptr && region->OnPointBirth(point))
			{
				// Put this region at the top of the list
				m_backgroundRegions.emplace_front(region);
				m_backgroundRegions.unique();
				bProcessed = true;
				break;
			}
		}
	}
}

void Engine::OnCursorUpdate(const TUIOCursor& cursor)
{
	bool bProcessed = false;

	TouchPoint point = cursor;
	for (auto& region : m_frontRegions)
	{
		if (region != nullptr && region->OnPointUpdate(point))
		{
			// Put this region at the top of the list
			m_frontRegions.emplace_front(region);
			m_frontRegions.unique();
			bProcessed = true;
			break;
		}
	}

	if (!bProcessed)
	{
		for (auto& region : m_backgroundRegions)
		{
			if (region != nullptr && region->OnPointUpdate(point))
			{
				// Put this region at the top of the list
				m_backgroundRegions.emplace_front(region);
				m_backgroundRegions.unique();
				bProcessed = true;
				break;
			}
		}
	}
}

void Engine::OnCursorDeath(const TUIOCursor& cursor)
{
	bool bProcessed = false;

	TouchPoint point = cursor;
	for (auto& region : m_frontRegions)
	{
		if (region != nullptr && region->OnPointDeath(point))
		{
			// Put this region at the top of the list
			m_frontRegions.emplace_front(region);
			m_frontRegions.unique();
			bProcessed = true;
			break;
		}
	}

	if (!bProcessed)
	{
		for (auto& region : m_backgroundRegions)
		{
			if (region != nullptr && region->OnPointDeath(point))
			{
				// Put this region at the top of the list
				m_backgroundRegions.emplace_front(region);
				m_backgroundRegions.unique();
				bProcessed = true;
				break;
			}
		}
	}
}

void Engine::OnRefresh()
{
	// TODO: Keep updating gestures
}

void Engine::RegisterRegion(Region* region, Priority priority)
{
	if (region != nullptr)
	{
		switch (priority)
		{
		case Priority::BACKGROUND:
			m_backgroundRegions.push_front(region);
			break;

		case Priority::FOREGROUND:
			m_frontRegions.push_front(region);
			break;
		}
	}
}

void Engine::UnregisterRegion(Region* region)
{
	if (region != nullptr)
	{
		m_frontRegions.remove(region);
		m_backgroundRegions.remove(region);
	}
}

} // namespace kim