// oscpkt library trigger secure-C-runtime warnings in Visual C++.
// Disable warnings locally
#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable: 4996)
#pragma warning(disable: 4267)
#pragma warning(disable: 4244)
#endif

#define OSCPKT_OSTREAM_OUTPUT
#include "oscpkt/oscpkt.hh"
#include "oscpkt/udp.hh"

#ifdef _MSC_VER
#pragma warning(pop)
#endif

#include "TUIOClient.h"

#include <thread>
#include <mutex>
#include <map>
#include <iostream>

namespace kim
{

using namespace oscpkt;
using namespace std;

class TUIOClient : public ATUIOClient
{
public:
	virtual bool Init() override;
	virtual void Run() override;
	virtual void Shutdown() override;

	bool Init(int port);

private:
	void Receive();

	void ReceiveObject(oscpkt::Message::ArgReader& arguments);
	void ReceiveCursor(oscpkt::Message::ArgReader& arguments);
	void ReceiveBlob(oscpkt::Message::ArgReader& arguments);

	bool IsTearingDown();
	void SetTearDown(bool bTearDown);

private:
	TUIOIDsContainer m_aliveCursorIds;
	TUIOContainer m_cursors;
	int m_currentFrame;
	int m_lastFrame;


	oscpkt::UdpSocket m_socket;
	int m_port;

	std::thread m_listeningThread;
	std::mutex m_mutex;

	bool m_bTearDown;

	static const int DEFAULT_PORT;
};

TouchPoint::TouchPoint(const TUIOCursor& cursor)
{
	id = cursor.id;
	x = cursor.x;
	y = cursor.y;
	X = cursor.X;
	Y = cursor.Y;
	m = cursor.m;
}

TouchPoint& TouchPoint::operator=(const TUIOCursor& cursor)
{
	id = cursor.id;
	x = cursor.x;
	y = cursor.y;
	X = cursor.X;
	Y = cursor.Y;
	m = cursor.m;

	return *this;
}

std::shared_ptr<ATUIOClient> ATUIOClient::CreateTUIOClient()
{
	return std::make_shared<TUIOClient>();
}

void ATUIOClient::RegisterListener(ITUIOEventListener* listener)
{
	if (listener != nullptr)
	{
		m_listeners.push_back(listener);
	}
}

void ATUIOClient::UnregisterListener(ITUIOEventListener* listener)
{
	if (listener != nullptr)
	{
		auto listenerPosition = std::find(m_listeners.begin(), m_listeners.end(), listener);
		if (listenerPosition != m_listeners.end())
		{
			m_listeners.erase(listenerPosition);
		}
	}
}

void ATUIOClient::NotifyCursorBirth(const TUIOCursor& cursor)
{
	for (const auto& listener : m_listeners)
	{
		listener->OnCursorBirth(cursor);
	}
}

void ATUIOClient::NotifyCursorUpdate(const TUIOCursor& cursor)
{
	for (const auto& listener : m_listeners)
	{
		listener->OnCursorUpdate(cursor);
	}
}

void ATUIOClient::NotifyCursorDeath(const TUIOCursor& cursor)
{
	for (const auto& listener : m_listeners)
	{
		listener->OnCursorDeath(cursor);
	}
}

void ATUIOClient::NotifyRefresh()
{
	for (const auto& listener : m_listeners)
	{
		listener->OnRefresh();
	}
}

const int TUIOClient::DEFAULT_PORT = 3333;

bool TUIOClient::Init()
{
	return Init(DEFAULT_PORT);
}

bool TUIOClient::Init(int port)
{
	m_bTearDown = false;
	m_port = port;
	m_currentFrame = -1;
	m_lastFrame = -1;
	m_cursors.clear();
	
	return true;
}

void TUIOClient::Run()
{
	m_listeningThread = thread(&TUIOClient::Receive, this);
}

void TUIOClient::Shutdown()
{
	SetTearDown(true);

	if (m_listeningThread.joinable())
	{
		m_listeningThread.join();
	}
}

bool TUIOClient::IsTearingDown()
{
	m_mutex.lock();
	bool bTearValue = m_bTearDown;
	m_mutex.unlock();

	return bTearValue;
}

void TUIOClient::SetTearDown(bool bTearDown)
{
	m_mutex.lock();
		m_bTearDown = bTearDown;
	m_mutex.unlock();
}

void TUIOClient::Receive()
{
	m_socket.bindTo(m_port);
	if (!m_socket.isOk())
	{
		cerr << "Error opening port " << m_port << ": " << m_socket.errorMessage() << "\n";
		return;
	}

	while (m_socket.isOk() && !IsTearingDown())
	{
		if (m_socket.receiveNextPacket(30 /* timeout, in ms */))
		{
			PacketReader pr;
			pr.init(m_socket.packetData(), m_socket.packetSize());
			oscpkt::Message *msg;
			while (pr.isOk() && (msg = pr.popMessage()) != 0)
			{
				// Currently supporting cursors. Other types are being converted to cursors.
				// TODO: Support all message types
				if (oscpkt::Message::ArgReader arguments = msg->partialMatch("/tuio/2Dobj"))
				{
					ReceiveObject(arguments);
				}
				else if (oscpkt::Message::ArgReader arguments = msg->partialMatch("/tuio/2Dcur"))
				{
					ReceiveCursor(arguments);
				}
				else if (oscpkt::Message::ArgReader arguments = msg->partialMatch("/tuio/2Dblb"))
				{
					ReceiveBlob(arguments);
				}
				else
				{
					cout << "Server: unhandled message: " << *msg << "\n";
				}
			}
		}
	}

	m_socket.close();
}

void TUIOClient::ReceiveObject(oscpkt::Message::ArgReader& arguments)
{
	std::string msgType;
	arguments = arguments.popStr(msgType);
	if (msgType == "alive" && m_currentFrame != m_lastFrame)
	{
		m_aliveCursorIds.clear();

		while (!arguments.isOkNoMoreArgs())
		{
			int aliveId;
			arguments = arguments.popInt32(aliveId);

			m_aliveCursorIds.emplace(aliveId, aliveId);
		}

		// Manage dead (non-alive) cursors
		vector<int> deadIDs;
		for (const auto& it : m_cursors)
		{
			int currentID = it.first;
			// if not found in the alive list, it must be dead
			if (m_aliveCursorIds.find(currentID) == m_aliveCursorIds.end())
			{
				deadIDs.push_back(currentID);
			}
		}
		for (const auto& it : deadIDs)
		{
			auto deadCursor = m_cursors[it];
			NotifyCursorDeath(deadCursor);

			m_cursors.erase(it);
		}
	}
	else if (msgType == "fseq")
	{
		m_lastFrame = m_currentFrame;

		arguments = arguments.popInt32(m_currentFrame);

		NotifyRefresh();

	}
	else if (msgType == "set" && m_currentFrame != m_lastFrame)
	{
		int s, i;
		float x, y, a, X, Y, A, m, r;

		if (arguments.popInt32(s).popInt32(i).popFloat(x).popFloat(y).popFloat(a).popFloat(X).popFloat(Y).popFloat(A).popFloat(m).popFloat(r).isOkNoMoreArgs())
		{
			// Create new cursor if it did not exist yet
			auto it = m_cursors.find(s);
			if (it == m_cursors.end())
			{
				TUIOCursor cursor;
				cursor.id = s;
				// Correct precision errors - clamp position to 0-1
				cursor.x = max(0, min(1, x));
				cursor.y = max(0, min(1, y));
				cursor.X = X;
				cursor.Y = Y;
				cursor.m = m;

				m_cursors.emplace(s, cursor);

				NotifyCursorBirth(cursor);
			}
			else
			{
				// Update existing cursor if position has changed
				TUIOCursor& cursor = it->second;
				if (cursor.x != x || cursor.y != y)
				{
					cursor.x = x;
					cursor.y = y;
					cursor.X = X;
					cursor.Y = Y;
					cursor.m = m;

					NotifyCursorUpdate(cursor);
				}
			}
		}
		else
		{
			std::cout << "Error: Set msg bad formed!" << std::endl;
		}
	}
}

void TUIOClient::ReceiveCursor(oscpkt::Message::ArgReader& arguments)
{
	std::string msgType;
	arguments = arguments.popStr(msgType);
	if (msgType == "alive" && m_currentFrame != m_lastFrame)
	{
		m_aliveCursorIds.clear();

		while (!arguments.isOkNoMoreArgs())
		{
			int aliveId;
			arguments = arguments.popInt32(aliveId);

			m_aliveCursorIds.emplace(aliveId, aliveId);
		}

		// Manage dead (non-alive) cursors
		vector<int> deadIDs;
		for (const auto& it : m_cursors)
		{
			int currentID = it.first;
			// if not found in the alive list, it must be dead
			if (m_aliveCursorIds.find(currentID) == m_aliveCursorIds.end())
			{
				deadIDs.push_back(currentID);
			}
		}
		for (const auto& it : deadIDs)
		{
			auto deadCursor = m_cursors[it];
			NotifyCursorDeath(deadCursor);

			m_cursors.erase(it);
		}
	}
	else if (msgType == "fseq")
	{
		m_lastFrame = m_currentFrame;

		arguments = arguments.popInt32(m_currentFrame);

		NotifyRefresh();

	}
	else if (msgType == "set" && m_currentFrame != m_lastFrame)
	{
		int id;
		float x, y, vx, vy, mAcc;

		if (arguments.popInt32(id).popFloat(x).popFloat(y).popFloat(vx).popFloat(vy).popFloat(mAcc).isOkNoMoreArgs())
		{
			// Create new cursor if it did not exist yet
			auto it = m_cursors.find(id);
			if (it == m_cursors.end())
			{
				TUIOCursor cursor;
				cursor.id = id;
				// Correct precision errors - clamp position to 0-1
				cursor.x = max(0, min(1, x));
				cursor.y = max(0, min(1, y));
				cursor.X = vx;
				cursor.Y = vy;
				cursor.m = mAcc;

				m_cursors.emplace(id, cursor);

				NotifyCursorBirth(cursor);
			}
			else
			{
				// Update existing cursor if position has changed
				TUIOCursor& cursor = it->second;
				if (cursor.x != x || cursor.y != y)
				{
					cursor.x = x;
					cursor.y = y;
					cursor.X = vx;
					cursor.Y = vy;
					cursor.m = mAcc;

					NotifyCursorUpdate(cursor);
				}
			}
		}
		else
		{
			std::cout << "Error: Set msg bad formed!" << std::endl;
		}
	}
}

void TUIOClient::ReceiveBlob(oscpkt::Message::ArgReader& arguments)
{
	std::string msgType;
	arguments = arguments.popStr(msgType);
	if (msgType == "alive" && m_currentFrame != m_lastFrame)
	{
		m_aliveCursorIds.clear();

		while (!arguments.isOkNoMoreArgs())
		{
			int aliveId;
			arguments = arguments.popInt32(aliveId);

			m_aliveCursorIds.emplace(aliveId, aliveId);
		}

		// Manage dead (non-alive) cursors
		vector<int> deadIDs;
		for (const auto& it : m_cursors)
		{
			int currentID = it.first;
			// if not found in the alive list, it must be dead
			if (m_aliveCursorIds.find(currentID) == m_aliveCursorIds.end())
			{
				deadIDs.push_back(currentID);
			}
		}
		for (const auto& it : deadIDs)
		{
			auto deadCursor = m_cursors[it];
			NotifyCursorDeath(deadCursor);

			m_cursors.erase(it);
		}
	}
	else if (msgType == "fseq")
	{
		m_lastFrame = m_currentFrame;

		arguments = arguments.popInt32(m_currentFrame);

		NotifyRefresh();

	}
	else if (msgType == "set" && m_currentFrame != m_lastFrame)
	{
		int s;
		float x, y, a, w, h, f, X, Y, A, m, r;

		if (arguments.popInt32(s).popFloat(x).popFloat(y).popFloat(a).popFloat(w).popFloat(h).popFloat(f).popFloat(X).popFloat(Y).popFloat(A).popFloat(m).popFloat(r).isOkNoMoreArgs())
		{
			// Create new cursor if it did not exist yet
			auto it = m_cursors.find(s);
			if (it == m_cursors.end())
			{
				TUIOCursor cursor;
				cursor.id = s;
				// Correct precision errors - clamp position to 0-1
				cursor.x = max(0, min(1, x));
				cursor.y = max(0, min(1, y));
				cursor.X = X;
				cursor.Y = Y;
				cursor.m = m;

				m_cursors.emplace(s, cursor);

				NotifyCursorBirth(cursor);
			}
			else
			{
				// Update existing cursor if position has changed
				TUIOCursor& cursor = it->second;
				if (cursor.x != x || cursor.y != y)
				{
					cursor.x = x;
					cursor.y = y;
					cursor.X = X;
					cursor.Y = Y;
					cursor.m = m;

					NotifyCursorUpdate(cursor);
				}
			}
		}
		else
		{
			std::cout << "Error: Set msg bad formed!" << std::endl;
		}
	}
}

} // namespace kim