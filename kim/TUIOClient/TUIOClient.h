#ifndef TUIOCLIENT_H
#define TUIOCLIENT_H

#include <memory>
#include <map>
#include "../kim.h"

namespace kim
{
typedef struct TUIOCursor
{
	int id;
	float x;
	float y;
	float X; // vx
	float Y; // vy
	float m; // motion acceleration
} TUIOCursor;

typedef std::map<int, TUIOCursor> TUIOContainer;
typedef std::map<int, int> TUIOIDsContainer;

class ITUIOEventListener
{
public:
	virtual void OnCursorBirth(const TUIOCursor& cursor) = 0;
	virtual void OnCursorUpdate(const TUIOCursor& cursor) = 0;
	virtual void OnCursorDeath(const TUIOCursor& cursor) = 0;
	virtual void OnRefresh() = 0;

	virtual ~ITUIOEventListener() {}
};

class ITUIOObservable
{
public:	
	virtual void RegisterListener(ITUIOEventListener* listener) = 0;
	virtual void UnregisterListener(ITUIOEventListener* listener) = 0;

	virtual ~ITUIOObservable() {}

protected:
	virtual void NotifyCursorBirth(const TUIOCursor& cursor) = 0;
	virtual void NotifyCursorUpdate(const TUIOCursor& cursor) = 0;
	virtual void NotifyCursorDeath(const TUIOCursor& cursor) = 0;
	virtual void NotifyRefresh() = 0;
};

class ATUIOClient : public IComponent, public ITUIOObservable
{
public:
	static std::shared_ptr<ATUIOClient> CreateTUIOClient();

	virtual ~ATUIOClient() {}

	virtual void RegisterListener(ITUIOEventListener* listener) override;
	virtual void UnregisterListener(ITUIOEventListener* listener) override;

protected:
	virtual void NotifyCursorBirth(const TUIOCursor& cursor) override;
	virtual void NotifyCursorUpdate(const TUIOCursor& cursor) override;
	virtual void NotifyCursorDeath(const TUIOCursor& cursor) override;
	virtual void NotifyRefresh() override;

private:
	std::vector<ITUIOEventListener*> m_listeners;
};

} // namespace kim

#endif // TUIOCLIENT_H