#include "kim.h"

namespace kim
{
	TouchPoint::TouchPoint()
		: id(ID_INVALID)
	{
	}

	bool TouchPoint::IsValid() const
	{
		return id != ID_INVALID;
	}

	void TouchPoint::Clear()
	{
		memset(this, 0, sizeof(TouchPoint));
	}

	float TouchPoint::Length() const
	{
		return std::sqrt(x*x + y*y);
	}

	bool operator==(const TouchPoint& lhs, const TouchPoint& rhs)
	{
		return lhs.id == rhs.id;
	}

	float Distance(const TouchPoint& a, const TouchPoint& b)
	{
		return std::sqrt((a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y));
	}
} // namespace kim