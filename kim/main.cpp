#include <memory>
#include <iostream>
#include "kim.h"

class TestRegion : public kim::Region
{
public:
	TestRegion(float x1, float y1, float x2, float y2)
		: Region(x1, y1, x2, y2)
	{
	}

	virtual void OnGestureCompleted(kim::Gesture* g) override
	{
		kim::DoubleClickGesture* dbc = dynamic_cast<kim::DoubleClickGesture*>(g);
		if (dbc != nullptr)
		{
			std::cout << "Received DoubleClickGesture at (" << dbc->GetX() << ", " << dbc->GetY() << ")" << std::endl;
		}

		kim::ZoomPinchGesture* zoom = dynamic_cast<kim::ZoomPinchGesture*>(g);
		if (zoom != nullptr)
		{
			std::cout << "Received ZoomPinchGesture - Scale: " << zoom->GetScale() << ", Center: (" << zoom->GetX() << ", " << zoom->GetY() << ")" << std::endl;
		}

		kim::RotateGesture* rot = dynamic_cast<kim::RotateGesture*>(g);
		if (rot != nullptr)
		{
			std::cout << "Received RotateGesture - Angle: " << rot->GetAngle() << ", Center: (" << rot->GetX() << ", " << rot->GetY() << ")" << std::endl;
		}

		kim::DragGesture* drag = dynamic_cast<kim::DragGesture*>(g);
		if (drag != nullptr)
		{
			std::cout << "Received DragGesture - Center: (" << drag->GetX() << ", " << drag->GetY() << ")" << std::endl;
		}
	}
};

int main(int argc, char** argv)
{
	TestRegion region(0, 0, 1, 1);
	auto dbcGesture = std::make_shared<kim::DoubleClickGesture>();
	auto zoomGesture = std::make_shared<kim::ZoomPinchGesture>();
	auto rotGesture = std::make_shared<kim::RotateGesture>();
	auto dragGesture = std::make_shared<kim::DragGesture>();

	region.AddAllowedGesture(dbcGesture);
	region.AddAllowedGesture(zoomGesture);
	region.AddAllowedGesture(rotGesture);
	region.AddAllowedGesture(dragGesture);

	auto engine = kim::IEngine::CreateEngine();
	if (engine->Init())
	{
		engine->RegisterRegion(&region);
		engine->Run();
		while (true);
		engine->Shutdown();
	}

	return 0;
}