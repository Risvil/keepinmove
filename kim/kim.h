#ifndef KIM_H
#define KIM_H

#include <vector>
#include <memory>
#include <chrono>

namespace kim
{
	struct TUIOCursor;
	typedef struct TouchPoint
	{
		enum { ID_INVALID = 0 };

		TouchPoint();
		TouchPoint(const TUIOCursor& cursor);
		TouchPoint& operator=(const TUIOCursor& cursor);

		bool IsValid() const;
		void Clear();

		float Length() const;

		int id;
		float x;
		float y;
		float X;
		float Y;
		float m;
	} TouchPoint;

	bool operator==(const TouchPoint& lhs, const TouchPoint& rhs);
	float Distance(const TouchPoint& a, const TouchPoint& b);

	class IComponent
	{
	public:
		virtual bool Init() = 0;
		virtual void Run() = 0;
		virtual void Shutdown() = 0;

		virtual ~IComponent() {}
	};

	class Region;
	class IEngine : public IComponent
	{
	public:
		static std::shared_ptr<IEngine> CreateEngine();

		enum class Priority { BACKGROUND, FOREGROUND };

		virtual bool Init() = 0;
		virtual void Run() = 0;
		virtual void Shutdown() = 0;

		virtual void RegisterRegion(Region* region, Priority priority) = 0;
		virtual void UnregisterRegion(Region* region) = 0;

		virtual ~IEngine() {}
	};

	struct TouchEvent
	{
		int pointID;
		float x;
		float y;
	};

	class Gesture;
	class IGestureListener
	{
	public:
		virtual void OnGesturePerformed(Gesture* gesture) = 0;

		virtual ~IGestureListener() {}
	};

	class Region : public IGestureListener
	{
	public:
		Region();
		Region(float x1, float y1, float x2, float y2);
		virtual ~Region();

		void SetSize(float x1, float y1, float x2, float y2);

		void AddAllowedGesture(const std::shared_ptr<Gesture>& gesture);
		void RemoveAllowedGesture(const std::shared_ptr<Gesture>& gesture);

		virtual void OnGesturePerformed(Gesture* gesture) override;

		bool operator==(const Region& rhs) const;

		// Gesture response events
		virtual void OnTouchDown(const TouchEvent& e) {}
		virtual void OnTouchUp(const TouchEvent& e) {}
		virtual void OnTouchMove(const TouchEvent& e) {}
		virtual void OnGestureCompleted(Gesture* g) {}

		virtual bool OnPointBirth(const TouchPoint& point);
		virtual bool OnPointUpdate(const TouchPoint& point);
		virtual bool OnPointDeath(const TouchPoint& point);

		float GetX1() const;
		float GetY1() const;
		float GetX2() const;
		float GetY2() const;

	protected:
		void AddPoint(const TouchPoint& point);
		void RemovePoint(const TouchPoint& point);
		bool HasPoint(const TouchPoint& point) const;
		bool IsInside(const TouchPoint& point) const;

	protected:
		int m_id;

		// Normalized region size (0-1)
		// Top left corner
		float m_x1;
		float m_y1;
		// Bottom down corner
		float m_x2;
		float m_y2;

	private:
		std::vector<TouchPoint> m_points;
		std::vector<std::shared_ptr<Gesture>> m_allowedGestures;
	};

	class Gesture
	{
	public:
		Gesture();
		virtual ~Gesture();

		virtual void OnPointBirth(const TouchPoint& point) {}
		virtual void OnPointUpdate(const TouchPoint& point) {}
		virtual void OnPointDeath(const TouchPoint& point) {}

		void AddGestureListener(IGestureListener* listener);
		void RemoveGestureListener(IGestureListener* listener);

		bool operator==(const Gesture& rhs) const;

	protected:
		void NotifyGesturePerformed();

	private:
		int m_id;
		std::vector<IGestureListener*> m_listeners;
	};

	class DoubleClickGesture : public Gesture
	{
	public:
		DoubleClickGesture(
			const std::chrono::milliseconds& allowedTimeBetweenClicks = std::chrono::milliseconds(1000),
			const std::chrono::milliseconds& allowedClickPressTime = std::chrono::milliseconds(1000),
			float allowedClickOffset = 0.1f
			);

		virtual void OnPointBirth(const TouchPoint& point) override;
		virtual void OnPointUpdate(const TouchPoint& point) override;
		virtual void OnPointDeath(const TouchPoint& point) override;

		void SetAllowedTimeBetweenClicks(const std::chrono::milliseconds& time);
		const std::chrono::milliseconds& GetAllowedTimeBetweenClicks() const;

		void SetAllowedClickOffset(float offset);
		float GetAllowedClickOffset() const;

		void SetAllowedClickPressTime(const std::chrono::milliseconds& time);
		const std::chrono::milliseconds& GetAllowedClickPressTime() const;

		float GetX() const;
		float GetY() const;

	private:
		void Reset();

	private:
		int m_clickCounter;
		std::chrono::steady_clock::time_point m_lastClickTime;
		TouchPoint m_lastClick;

		std::chrono::milliseconds m_allowedTimeBetweenClicks;
		std::chrono::milliseconds m_allowedClickPressTime;
		float m_allowedClickOffset;
	};

	class ADynamicGesture : public Gesture
	{
	public:
		ADynamicGesture();
		virtual ~ADynamicGesture();

		virtual void OnPointBirth(const TouchPoint& point) override;
		virtual void OnPointUpdate(const TouchPoint& point) override;
		virtual void OnPointDeath(const TouchPoint& point) override;

		virtual float GetX() const;
		virtual float GetY() const;

	protected:
		virtual void Reset() = 0;
		virtual void UpdateCentroid() = 0;

		void Refresh();

	protected:
		float m_centroidX;
		float m_centroidY;

		std::chrono::steady_clock::time_point m_lastUpdateTime;
		std::chrono::milliseconds m_allowedInactivityTime;
	};

	class ZoomPinchGesture : public ADynamicGesture
	{
	public:
		ZoomPinchGesture() = default;

		virtual void OnPointBirth(const TouchPoint& point) override;
		virtual void OnPointUpdate(const TouchPoint& point) override;
		virtual void OnPointDeath(const TouchPoint& point) override;

		float GetScale() const;
		float GetDeltaScale() const;

	private:
		virtual void Reset() override;
		virtual void UpdateCentroid() override;

	private:
		float m_scale;
		float m_deltaScale;
		float m_initialPointDistance;

		TouchPoint m_point1;
		TouchPoint m_point2;
	};

	class RotateGesture : public ADynamicGesture
	{
	public:
		RotateGesture() = default;

		virtual void OnPointBirth(const TouchPoint& point) override;
		virtual void OnPointUpdate(const TouchPoint& point) override;
		virtual void OnPointDeath(const TouchPoint& point) override;

		float GetAngle() const;
		float GetDeltaAngle() const;

	private:
		virtual void Reset() override;
		virtual void UpdateCentroid() override;

	private:
		float m_angle;
		float m_deltaAngle;
		float m_initialDirectionX;
		float m_initialDirectionY;
		TouchPoint m_point1;
		TouchPoint m_point2;
	};

	class DragGesture : public ADynamicGesture
	{
	public:
		virtual void OnPointBirth(const TouchPoint& point) override;
		virtual void OnPointUpdate(const TouchPoint& point) override;
		virtual void OnPointDeath(const TouchPoint& point) override;

	private:
		virtual void Reset() override;
		virtual void UpdateCentroid() override;

	private:
		TouchPoint m_point;
	};
}

#endif // KIM_H