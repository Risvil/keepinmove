#include "kim.h"

namespace kim
{

Region::Region()
: Region(0, 0, 0, 0)
{
}

Region::Region(float x1, float y1, float x2, float y2)
:	m_x1(x1),
	m_y1(y1),
	m_x2(x2),
	m_y2(y2)
{
	static int nextID = 0;

	m_id = nextID++;
}

Region::~Region()
{
}

void Region::SetSize(float x1, float y1, float x2, float y2)
{
	m_x1 = x1;
	m_y1 = y1;
	m_x2 = x2;
	m_y2 = y2;
}

void Region::AddAllowedGesture(const std::shared_ptr<Gesture>& gesture)
{
	if (gesture != nullptr)
	{
		gesture->AddGestureListener(this);
		m_allowedGestures.push_back(gesture);
	}
}

void Region::RemoveAllowedGesture(const std::shared_ptr<Gesture>& gesture)
{
	auto gestureLocation = std::find(m_allowedGestures.begin(), m_allowedGestures.end(), gesture);
	if (gestureLocation != m_allowedGestures.end())
	{
		m_allowedGestures.erase(gestureLocation);
	}
}

void Region::OnGesturePerformed(Gesture* gesture)
{
	OnGestureCompleted(gesture);
}

bool Region::OnPointBirth(const TouchPoint& point)
{
	bool bCaptured = false;

	if (IsInside(point) && !HasPoint(point))
	{
		AddPoint(point);

		TouchEvent te;
		te.pointID = point.id;
		te.x = point.x;
		te.y = point.y;
		OnTouchDown(te);

		for (auto& gesture : m_allowedGestures)
		{
			if (gesture != nullptr)
			{
				gesture->OnPointBirth(point);
			}
		}

		bCaptured = true;
	}

	return bCaptured;
}

bool Region::OnPointUpdate(const TouchPoint& point)
{
	bool bCaptured = false;

	if (IsInside(point) && HasPoint(point))
	{
		TouchEvent te;
		te.pointID = point.id;
		te.x = point.x;
		te.y = point.y;
		OnTouchMove(te);

		for (auto& gesture : m_allowedGestures)
		{
			if (gesture != nullptr)
			{
				gesture->OnPointUpdate(point);
			}
		}

		bCaptured = true;
	}

	return bCaptured;
}

bool Region::OnPointDeath(const TouchPoint& point)
{
	bool bCaptured = false;

	if (IsInside(point) && HasPoint(point))
	{
		RemovePoint(point);

		TouchEvent te;
		te.pointID = point.id;
		te.x = point.x;
		te.y = point.y;
		OnTouchUp(te);

		for (auto& gesture : m_allowedGestures)
		{
			if (gesture != nullptr)
			{
				gesture->OnPointDeath(point);
			}
		}

		bCaptured = true;
	}

	return bCaptured;
}

float Region::GetX1() const
{
	return m_x1;
}

float Region::GetY1() const
{
	return m_y1;
}

float Region::GetX2() const
{
	return m_x2;
}

float Region::GetY2() const
{
	return m_y2;
}

bool Region::operator==(const Region& rhs) const
{
	return m_id == rhs.m_id;
}

void Region::AddPoint(const TouchPoint& point)
{
	TouchPoint p(point);
	m_points.push_back(p);
}

void Region::RemovePoint(const TouchPoint& point)
{
	auto pointLocation = std::find(m_points.begin(), m_points.end(), point);
	if (pointLocation != m_points.end())
	{
		m_points.erase(pointLocation);
	}
}

bool Region::HasPoint(const TouchPoint& point) const
{
	auto endIt = m_points.end();
	auto pointLocation = std::find(m_points.begin(), endIt, point);
	return pointLocation != endIt;
}

bool Region::IsInside(const TouchPoint& point) const
{
	return	point.x >= m_x1 && point.x <= m_x2 &&
		point.y >= m_y1 && point.y <= m_y2;
}

}