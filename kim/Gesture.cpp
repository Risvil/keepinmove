#include "kim.h"

namespace kim
{

Gesture::Gesture()
{
	static int nextGestureId = 0;

	m_id = nextGestureId++;
}

Gesture::~Gesture()
{
}

bool Gesture::operator==(const Gesture& rhs) const
{
	return m_id == rhs.m_id;
}

void Gesture::AddGestureListener(IGestureListener* listener)
{
	if (listener != nullptr)
	{
		m_listeners.push_back(listener);
	}
}

void Gesture::RemoveGestureListener(IGestureListener* listener)
{
	if (listener != nullptr)
	{
		auto listenerPosition = std::find(m_listeners.begin(), m_listeners.end(), listener);
		if (listenerPosition != m_listeners.end())
		{
			m_listeners.erase(listenerPosition);
		}
	}
}

void Gesture::NotifyGesturePerformed()
{
	for (auto& listener : m_listeners)
	{
		if (listener != nullptr)
		{
			listener->OnGesturePerformed(this);
		}
	}
}

DoubleClickGesture::DoubleClickGesture(
		const std::chrono::milliseconds& allowedTimeBetweenClicks,
		const std::chrono::milliseconds& allowedClickPressTime,
		float allowedClickOffset
		)

:	m_allowedTimeBetweenClicks(allowedTimeBetweenClicks),
	m_allowedClickPressTime(allowedClickPressTime),
	m_allowedClickOffset(allowedClickOffset),
	m_clickCounter(0)
{
}

void DoubleClickGesture::OnPointBirth(const TouchPoint& point)
{
	auto currentTime = std::chrono::steady_clock::now();

	if (m_clickCounter > 0)
	{
		auto elapsedTime = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime - m_lastClickTime);
		auto dx = std::abs(m_lastClick.x - point.x);
		auto dy = std::abs(m_lastClick.y - point.y);

		// Reset if click was performed too far away from last one or too late
		if (elapsedTime > m_allowedTimeBetweenClicks || dx > m_allowedClickOffset || dy > m_allowedClickOffset)
		{
			// Reset and add this point as the first one
			Reset();
		}
	}

	m_lastClick = point;
	m_lastClickTime = currentTime;
}

void DoubleClickGesture::OnPointUpdate(const TouchPoint& point)
{
}

void DoubleClickGesture::OnPointDeath(const TouchPoint& point)
{
	auto currentTime = std::chrono::steady_clock::now();
	auto elapsedTime = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime - m_lastClickTime);

	if (point == m_lastClick && elapsedTime <= m_allowedClickPressTime)
	{
		++m_clickCounter;
	}
	else
	{
		Reset();
	}
	
	if (m_clickCounter == 2)
	{
		NotifyGesturePerformed();

		// Gesture performed. Now reset
		Reset();
	}
}

void DoubleClickGesture::SetAllowedTimeBetweenClicks(const std::chrono::milliseconds& time)
{
	m_allowedTimeBetweenClicks = time;
}

const std::chrono::milliseconds& DoubleClickGesture::GetAllowedTimeBetweenClicks() const
{
	return m_allowedTimeBetweenClicks;
}

void DoubleClickGesture::SetAllowedClickOffset(float offset)
{
	m_allowedClickOffset = offset;
}

float DoubleClickGesture::GetAllowedClickOffset() const
{
	return m_allowedClickOffset;
}

void DoubleClickGesture::SetAllowedClickPressTime(const std::chrono::milliseconds& time)
{
	m_allowedClickPressTime = time;
}

const std::chrono::milliseconds& DoubleClickGesture::GetAllowedClickPressTime() const
{
	return m_allowedClickPressTime;
}

float DoubleClickGesture::GetX() const
{
	return m_lastClick.x;
}

float DoubleClickGesture::GetY() const
{
	return m_lastClick.y;
}

void DoubleClickGesture::Reset()
{
	m_clickCounter = 0;
}

ADynamicGesture::ADynamicGesture()
:	m_allowedInactivityTime(500),
	m_centroidX(0),
	m_centroidY(0)
{
}

void ADynamicGesture::OnPointBirth(const TouchPoint& point)
{
	Gesture::OnPointBirth(point);

	// Check if gesture has expired
	auto currentTime = std::chrono::steady_clock::now();
	auto elapsedTime = currentTime - m_lastUpdateTime;
	if (elapsedTime > m_allowedInactivityTime)
	{
		Reset();
	}
}

ADynamicGesture::~ADynamicGesture()
{
}

void ADynamicGesture::OnPointUpdate(const TouchPoint& point)
{
	Gesture::OnPointUpdate(point);
}

void ADynamicGesture::OnPointDeath(const TouchPoint& point)
{
	Gesture::OnPointDeath(point);
}

float ADynamicGesture::GetX() const
{
	return m_centroidX;
}

float ADynamicGesture::GetY() const
{
	return m_centroidY;
}

void ADynamicGesture::Refresh()
{
	m_lastUpdateTime = std::chrono::steady_clock::now();
}

void ZoomPinchGesture::OnPointBirth(const TouchPoint& point)
{
	ADynamicGesture::OnPointBirth(point);

	bool bPointChanged = false;
	if (!m_point1.IsValid())
	{
		m_point1 = point;
		bPointChanged = true;
	}
	else if (!m_point2.IsValid())
	{
		m_point2 = point;
		bPointChanged = true;
	}

	if (bPointChanged)
	{
		m_initialPointDistance = Distance(m_point1, m_point2);

		Refresh();
	}
}

void ZoomPinchGesture::OnPointUpdate(const TouchPoint& point)
{
	ADynamicGesture::OnPointUpdate(point);

	if (m_point1.IsValid() && m_point2.IsValid())
	{
		if (point == m_point1)
		{
			m_point1 = point;
		}
		else if (point == m_point2)
		{
			m_point2 = point;
		}
		else
		{
			return;
		}

		float newDistance = Distance(m_point1, m_point2);		
		float newScale = newDistance / m_initialPointDistance;

		// Check for noise
		if (std::abs(newScale - m_scale) >= 0.1)
		{
			m_deltaScale = newScale - m_scale;
			m_scale = newScale;

			UpdateCentroid();
			NotifyGesturePerformed();
		}

		Refresh();
	}
}

void ZoomPinchGesture::OnPointDeath(const TouchPoint& point)
{
	ADynamicGesture::OnPointDeath(point);

	if (point == m_point2)
	{
		m_point2.Clear();
	}
	else if (point == m_point1)
	{
		m_point2.Clear();
	}
}

float ZoomPinchGesture::GetScale() const
{
	return m_scale;
}

float ZoomPinchGesture::GetDeltaScale() const
{
	return m_deltaScale;
}

void ZoomPinchGesture::Reset()
{
	m_scale = 1;
	m_initialPointDistance = 1;
	m_point1.Clear();
	m_point2.Clear();
}

void ZoomPinchGesture::UpdateCentroid()
{
	m_centroidX = (m_point1.x + m_point2.x) / 2;
	m_centroidY = (m_point1.y + m_point2.y) / 2;
}

void RotateGesture::OnPointBirth(const TouchPoint& point)
{
	ADynamicGesture::OnPointBirth(point);

	bool bPointChanged = false;
	if (!m_point1.IsValid())
	{
		m_point1 = point;
		bPointChanged = true;
	}
	else if (!m_point2.IsValid())
	{
		m_point2 = point;
		bPointChanged = true;
	}

	if (bPointChanged)
	{
		m_initialDirectionX = m_point2.x - m_point1.x;
		m_initialDirectionY = m_point2.y - m_point1.y;

		Refresh();
	}
}

void RotateGesture::OnPointUpdate(const TouchPoint& point)
{
	ADynamicGesture::OnPointUpdate(point);

	if (m_point1.IsValid() && m_point2.IsValid())
	{
		if (point == m_point1)
		{
			m_point1 = point;
		}
		else if (point == m_point2)
		{
			m_point2 = point;
		}
		else
		{
			return;
		}

		UpdateCentroid();

		float directionX = m_point2.x - m_point1.x;
		float directionY = m_point2.y - m_point1.y;

		float angle = atan2(directionY, directionX) - atan2(m_initialDirectionY, m_initialDirectionX);

		float radDeltaAngle = angle * 180.f / 3.14159265f;
		float angleDiff = std::abs(m_angle - radDeltaAngle);

		if (angleDiff > 2)
		{
			m_deltaAngle = m_angle - radDeltaAngle;
			m_angle = radDeltaAngle;
			NotifyGesturePerformed();
		}

		Refresh();
	}
}

void RotateGesture::OnPointDeath(const TouchPoint& point)
{
	ADynamicGesture::OnPointDeath(point);

	if (point == m_point2)
	{
		m_point2.Clear();
	}
	else if (point == m_point1)
	{
		m_point2.Clear();
	}
}

void RotateGesture::UpdateCentroid()
{
	m_centroidX = (m_point1.x + m_point2.x) / 2;
	m_centroidY = (m_point1.y + m_point2.y) / 2;
}

float RotateGesture::GetAngle() const
{
	return m_angle;
}

float RotateGesture::GetDeltaAngle() const
{
	return m_deltaAngle;
}


void RotateGesture::Reset()
{
	m_point1.Clear();
	m_point2.Clear();
	m_angle = 0.f;
}

void DragGesture::OnPointBirth(const TouchPoint& point)
{
	ADynamicGesture::OnPointBirth(point);

	if (m_point.IsValid())
	{
		Reset();
	}
	else
	{
		m_point = point;
		Refresh();
	}
}

void DragGesture::OnPointUpdate(const TouchPoint& point)
{
	ADynamicGesture::OnPointUpdate(point);

	if (point == m_point)
	{
		float distance = Distance(m_point, point);
		if (distance > 0.02)
		{
			m_point = point;
			UpdateCentroid();
			NotifyGesturePerformed();
		}

		Refresh();
	}
}

void DragGesture::OnPointDeath(const TouchPoint& point)
{
	ADynamicGesture::OnPointDeath(point);

	if (point == m_point)
	{
		Reset();
	}
}

void DragGesture::Reset()
{
	m_point.Clear();
}

void DragGesture::UpdateCentroid()
{
	m_centroidX = m_point.x;
	m_centroidY = m_point.y;
}

}